'use strict';

/** @type {import('sequelize-cli').Migration} */
module.exports = {
  async up (queryInterface, DataTypes) {
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
    await queryInterface.createTable('Tasks', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: DataTypes.STRING,
        description: DataTypes.STRING,
        status: DataTypes.STRING,
        priority: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                min: 0,
                max: 5
            }
        },
        storyPoints: {
              type: DataTypes.INTEGER,
              defaultValue: 0,
              validate: {
                  min: 0,
                  max: 5
              }
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        }
    });
    await queryInterface.bulkInsert('Tasks', [
      {
        title: 'Task 1',
        description: 'Task 1 description',
        status: 'TO_DO',
        priority: 0,
        storyPoints: 3,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 2',
        description: 'Task 2 description',
        status: 'TO_DO',
        priority: 0,
        storyPoints: 3,
        createdAt: new Date(),
        updatedAt: new Date()

      },
      {
        title: 'Task 3',
        description: 'Task 3 description',
        status: 'TO_DO',
        priority: 0,
        storyPoints: 7,
        createdAt: new Date(),
        updatedAt: new Date()
      },
      {
        title: 'Task 4',
        description: 'Task 4 description',
        status: 'TO_DO',
        priority: 0,
        storyPoints: 7,
        createdAt: new Date(),
        updatedAt: new Date()
      }
    ], {});
  },

  async down (queryInterface, Sequelize) {
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
    await queryInterface.bulkDelete('Tasks', null, {});
  }
};
