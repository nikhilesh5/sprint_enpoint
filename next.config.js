const withPreact = require('next-plugin-preact');

/** @type {import('next').NextConfig} */
const nextConfig = {
  async redirects() {
    return [
      {
        source: '/',
        destination: '/tasks',
        permanent: true,
      },
    ]
  },
  reactStrictMode: true,
  productionBrowserSourceMaps: true,
  experimental: {esmExternals: false} // FIXME: this is workaround for the issue on https://stackoverflow.com/a/60250115
}

module.exports = withPreact(nextConfig)
