
export function tasksModel(sequelize, DataTypes){
    const Task = sequelize.define('Task', {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        title: {
            type: DataTypes.STRING,
            allowNull: false,
        },
        description: {
            type: DataTypes.STRING,
            defaultValue: ''
        },
        status: {
            type: DataTypes.STRING,
            defaultValue: 'TO_DO',
        },
        priority: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                min: {
                    args: [0],
                    msg: 'Priority must be greater than 0'
                },
                max: {
                    args: [5],
                    msg: 'Priority must be less than 5'
                }
            }
        },
        storyPoints: {
            type: DataTypes.INTEGER,
            defaultValue: 0,
            validate: {
                min: {
                    args: [0],
                    msg: 'Story points must be greater than 0'
                },
                max: {
                    args: [20],
                    msg: 'Story points must be less than 20'
                }
            },
        },
        createdAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        },
        updatedAt: {
            type: DataTypes.DATE,
            defaultValue: DataTypes.NOW
        }
    }, {});
    Task.associate = function(models) {
        // associations can be defined here
    };
    return Task;
}