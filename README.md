# Setup 
1. Seed Database by running `sequelize db:seed:undo:all;sequelize db:seed:all `
2. Start `yarn dev`
3. To debug `NODE_OPTIONS='--inspect=0' yarn dev `. `--inspect=0` is required.