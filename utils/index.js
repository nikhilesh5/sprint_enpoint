export function createResponse (res,data, status = 200) {
    const response = {
        status: status,
        data: data
    }
    res.status(status).json(response);
}

export function createError (res, error, status = 500) {
    const response = {
        status: status,
        error: error
    }
    res.status(status).json(response);
}
function addDays(date, days) {
    const result = new Date(date);
    result.setDate(result.getDate() + days);
    return result;
}
// based on story points, where each task.storyPoint is 1 day.
// exclude weekdays
// @returns array of dates with corresponding tasks
export function transformTasksToTimeline({tasks, startDate = new Date(), sortByPriority = false}) {
    const timeline = [];
    let date = startDate;
    if(sortByPriority) {
        tasks = [...tasks].sort((a,b)=>b.priority-a.priority)
    }
    tasks.forEach((task) => {
        const {storyPoints} = task;
        for(let i = 0; i < storyPoints; i++) {
            // continue if weekend
            while(date.getDay() === 0 || date.getDay() === 6) {
                date = addDays(date,1);
                timeline.push()
            }
            timeline.push({
                priority: task.storyPoints,
                date: new Date(date),
                taskId: task.id,
                __task__: task,
                index:timeline.length
            })
            date = addDays(date,1);
        }
    })
    return timeline;
}

// get  uniq color for a string
export function stringToHslColor(str, s=30, l=80) {
    str = JSON.stringify(str);
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 5) - hash);
    }

    let h = hash % 360;
    return 'hsl('+h+', '+s+'%, '+l+'%)';
}
export function getRedToGreen(value) {
    //value from 0 to 5
    let hue = ((1 - value / 5) * 120).toString(10);
    return ["hsl(", hue, " 100% 50%)"].join("");
}

// get first date and last date of the task
export function aggregateTimeLineByTaskDate(timeLine) {
    const tasks = {};
    timeLine.forEach((item) => {
        if (!tasks[item.taskId]) {
            tasks[item.taskId] = {
                ...item,
                date: [item.date],
            }
        } else {
            tasks[item.taskId].date.push(item.date)
        }
    })
    return Object.values(tasks).sort((a, b) => a.date[0] - b.date[0]);
}