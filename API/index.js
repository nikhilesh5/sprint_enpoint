import axios from 'axios';
export function createTask(task){
    return axios.post('/api/tasks/create', task)
}
export async function getTasks(){
    const list = await axios.get('/api/tasks')
    if(list.data.status !== 200) {
        throw new Error(list.data.error)
    }
    return list.data.data
}