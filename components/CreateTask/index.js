import {useMemo, useRef} from "react";
import {useQuery} from "@tanstack/react-query";
import {getTasks} from "../../API";
import {TaskInput} from "./TaskInput";
import {renderListItem} from "./RenderListItem";


export function CreateTask() {
    const inputRef = useRef()


    const query = useQuery(['tasks'],getTasks)
    const tasks = useMemo(()=>query?.data?.map(a=>a).reverse()||[],[query.data])
    const addTodo = () => {
        // TODO: not tested
        if (this.todoTitle.trim() === "") {
            return;
        }
        this.todos.push({
            id: this.todoId,
            title: this.todoTitle,
            isComplete: false
        });

        this.todoId++;
        this.todoTitle = "";
    }
    const deleteTodo = (id) => {
        // TODO: not tested
        this.todos = this.todos.filter((todo) => id !== todo.id);
    }
    return (<>
            <span class="p-4 bg-[var(--color-a)] text-[white] ">

                    <TaskInput type="text" placeholder="task title"
                               class=" rounded-sm px-4 py-2 border border-gray-200 w-full mt-4"
                               x-model="todoTitle" ref={inputRef} keydown="addTodo()"/>
            </span>
            <div class="container px-3 h-full">
                <div class="bg-white rounded shadow px-4 py-4">

                    <ul class="todo-list mt-4 flex flex-col">
                        {tasks.map(task => renderListItem(task))}
                    </ul>
                </div>


            </div>
        </>
    )
}