import {useState} from "react";
import {useMutation, useQueryClient} from "@tanstack/react-query";
import {createTask} from "../../API";
import {getRedToGreen} from "../../utils";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faPlus} from "@fortawesome/free-solid-svg-icons";

export const TaskInput = (inputProps) => {
    const [task, setTask] = useState({
        priority: 2
    });
    const queryClient = useQueryClient()

    const mutation = useMutation({
        mutationFn: createTask,
        onSuccess: () => {
            queryClient.invalidateQueries({queryKey: ['tasks']});
            setTask({})
        },
    })
    const submitTask = (e) => {
        e.preventDefault();
        mutation.mutate(task)
    }
    return <form onSubmit={submitTask}>
        <label htmlFor="price" className="block text-sm font-medium text-gray-700">
            <div class="title font-bold text-lg">Add New Task</div>
        </label>
        <div className="relative mt-1 rounded-md p-4 flex flex-row">
            <input
                type="text"
                name="price"
                id="price"
                className="text-[black] block w-full rounded-md border-gray-300 pl-7 focus:outline-none pr-12 sm:text-sm"
                placeholder="0.00"
                value={task.title}
                onChange={(e) => setTask(t => ({...t, title: e.target.value}))}
                {...inputProps}
            />
            <span class="flex gap-4">
        <div className="inset-y-0 right-0 flex items-center">
            <label htmlFor="currency" className="sr-only">
                Story Points
            </label>
            <select
                style={{color: getRedToGreen(task.priority)}}
                value={task.priority}
                onChange={(e) => setTask(t => ({...t, priority: e.target.value}))}
                id="currency"
                name="currency"
                className="h-full rounded-md border-transparent bg-transparent py-0 pl-2 pr-7 text-gray-500 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
            >
                 {[0, 1, 2, 3, 4, 5].map(priority => <option style={{color: getRedToGreen(priority)}} value={priority}
                                                             key={priority} class="flex place-items-center gap-1">
                     &#xf15e;
                     <span>&nbsp;&nbsp;{priority}</span>
                 </option>)}
            </select>
        </div>
        <div className="inset-y-0 right-0 flex items-center">
            <label htmlFor="currency" className="sr-only">
                Currency
            </label>
            <select
                oninvalid={(e) => e.target.setCustomValidity('Please Select Story Points')}
                required
                id="currency"
                value={task.storyPoints}
                onChange={(e) => setTask(t => ({...t, storyPoints: e.target.value}))}
                name="currency"
                className="h-full rounded-md border-transparent bg-transparent py-0 pl-2 pr-7 text-gray-500 focus:border-indigo-500 focus:ring-indigo-500 sm:text-sm"
            >
                <option value="">&#xf017;&nbsp;&nbsp; - </option>
                {[0, 1, 2, 3, 4, 5].map(storyPoints => <option value={storyPoints} key={storyPoints}
                                                               className="flex place-items-center gap-1">
                    &#xf017;
                    <span>&nbsp;&nbsp;{storyPoints}</span>
                </option>)}
            </select>
        </div>
        <div className="inset-y-0 right-0 flex items-center">
            <button type='submit'
                    disabled={!task.title}
                    className="text-white rounded-lg shadow-lg bg-[var(--color-c)]  disabled:bg-[grey] p-2 pr-3 flex items-center">
                    <FontAwesomeIcon className="w-3 h-3 mr-3 focus:outline-none" icon={faPlus}/>
                    <span>Add</span>
                </button>
        </div>
            </span>
        </div>
    </form>
}