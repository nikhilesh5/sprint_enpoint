import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faArrowUp19} from "@fortawesome/free-solid-svg-icons";
import {getRedToGreen} from "../../utils";
import {faClock} from "@fortawesome/free-regular-svg-icons";

export const renderListItem = (task, deleteItem, setStatus) =>
    <li key={task.id} class="flex pt-4 justify-between items-center"
        x-show="todo.title !== ''">
        <div
            class={"flex w-full gap-2 content-center place-items-center " + (task.status === 'DONE' ? 'line-through' : '')}>
            <input disabled type="checkbox" name="" id="" onChange={() => setStatus(task.id, 'DONE')}/>
            <span>{task.id}</span>
            <div class="capitalize ml-3 text-sm font-semibold">{task.title}</div>
        </div>
        <div class="flex gap-5 flex-row" title="story points">
            <span class="flex place-items-center gap-1">
                <FontAwesomeIcon
                    className=" w-4 h-4 text-gray-600 opacity-50"
                    onclick={() => deleteItem(task.id)} fill="none" stroke-linecap="round"
                    icon={faArrowUp19}
                />
                <span style={{color: getRedToGreen(task.priority)}}>{task.priority}</span>
            </span>
            <span class="flex place-items-center gap-1">
                <FontAwesomeIcon
                    className=" w-4 h-4 text-gray-600 opacity-50"
                    onclick={() => deleteItem(task.id)} fill="none" stroke-linecap="round"
                    icon={faClock}
                />
                <span>{task.storyPoints}</span>
            </span>
            <button disabled onclick={() => deleteItem(task.id)}>
                <svg class=" w-4 h-4 text-gray-600 fill-current"
                     fill="none" stroke-linecap="round"
                     stroke-linejoin="round" stroke-width="2" viewBox="0 0 24 24"
                     stroke="currentColor">
                    <path d="M6 18L18 6M6 6l12 12"></path>
                </svg>
            </button>

        </div>
    </li>