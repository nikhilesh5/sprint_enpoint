import {useQuery} from "@tanstack/react-query";
import {getTasks} from "../../API";
import {aggregateTimeLineByTaskDate, transformTasksToTimeline} from "../../utils";
import {useMemo, useState} from "react";
import {renderTimelineRow} from "./renderTimelineRow";
import csvDownload from 'json-to-csv-export'
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import {faFileExport} from "@fortawesome/free-solid-svg-icons";
function exportTimeLineToCSV(timeline,filename='timeline.csv') {
    const csvData = timeline.map(t => ({
        taskId: t.__task__.id,
        title: t.__task__.title,
        priority: t.priority,
        storyPoints: t.storyPoints,
        startDate: t.date[0].toLocaleString(),
        endDate: t.date[t.date.length-1].toLocaleString()
    }));
    csvDownload({data: csvData, filename})
}
export default function TaskTimeLine(){
    const query = useQuery(['tasks'],getTasks);
    const [startDate,setStartDate]= useState('2023-01-01');
    const [byPriority,setByPriority] = useState(false);
    const timeline = useMemo(()=>
            aggregateTimeLineByTaskDate(
                transformTasksToTimeline({
                tasks:query?.data||[],
                startDate:new Date(startDate),
                sortByPriority:byPriority
        })),
        [query.data,startDate,byPriority])
        console.log(timeline);
    return (
        <>

            <span class=" relative p-4 bg-[var(--color-c)] flex gap-4 text-[white] text-[2em] w-full text-center place-items-center">
                <label htmlFor="priority" className="text-[1rem]">
                    Order by priority
                </label>
                <input id="priority" title='Order By priority' checked={byPriority}
                       onChange={(e) => setByPriority(e.target.checked)} type="checkbox"
                       className=" bg-[grey] ml-4 bg-[var(--color-b)] text-[white] w-[1em] h-[1em]"/>
                <label htmlFor="startDate" className="text-[1rem] float-right">
                    Start Date
                </label>
                <input id="startDate" value={startDate} onChange={(e) => setStartDate(e.target.value)} type="date"
                       className="float-right bg-[grey] ml-4 p-2 bg-[var(--color-b)] text-[white] text-[0.5em]"/>
                <FontAwesomeIcon title="Export as timeline.csv" icon={faFileExport} onClick={()=>exportTimeLineToCSV(timeline)} className=" float-right cursor-pointer w-[1em] h-[1em] absolute right-[1em]"/>
            </span>
            <table>
                <thead>
                    <tr>
                        <th>TaskId</th>
                        <th>Start Date</th>
                        <th>End Date</th>
                        <th>Title</th>
                        <th>Points</th>
                        <th>Priority</th>
                    </tr>
                </thead>
                <tbody>
                    {timeline.map(renderTimelineRow)}
                </tbody>
            </table>
        </>
    )
}
