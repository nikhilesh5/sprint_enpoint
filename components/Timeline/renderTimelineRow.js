import {stringToHslColor} from "../../utils";

const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];



export function renderTimelineRow(task) {
    return (
        <tr key={task.date.toString()} class="" style={{/*background: stringToHslColor(task.__task__)*/}}>
            <td className=" p-3 w-[3em] text-right">{task.taskId}</td>
            <td className="p-3 w-1/4 text-left pl-8">{`${days[task.date[0].getDay()]} ${task.date[0].toLocaleString()}`}</td>
            <td className="p-3 w-1/4 text-left pl-8">{`${days[task.date[task.date.length-1].getDay()]} ${task.date[task.date.length-1].toLocaleString()}`}</td>
            <td class="p-3 w-1/4 text-center">{task.__task__.title}</td>
            <td className="p-3 w-1/4 text-right">{task.__task__.storyPoints}</td>
            <td className="p-3 w-1/4 text-right">{task.__task__.priority}</td>
        </tr>
    )
}