import Head from 'next/head'
import styles from '../styles/Home.module.css'
import {CreateTask} from "../components/CreateTask";
import {faCloudSun} from "@fortawesome/free-solid-svg-icons";
import {FontAwesomeIcon} from "@fortawesome/react-fontawesome";
import Link from "next/link";
import TaskTimeLine from "../components/Timeline";
export default function Home({pageName}) {
    const Component = pageName === 'timeline' ? TaskTimeLine : CreateTask
    return (
    <div class="w-full">
      <Head>
        <title>Amusing Day</title>
        <meta name="description" content="Generated by create next app" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <header class="fixed w-full h-auto top-0">
          <nav class="flex items-center justify-between flex-wrap bg-[var(--color-e)] p-6">
              <div class="flex items-center flex-shrink-0 text-[var(--color-a)] mr-6">
                  <FontAwesomeIcon icon={faCloudSun} className="mr-4"/>
                  <span class="font-semibold text-xl tracking-tight">Amusing Tasks</span>
              </div>
              <div class="block lg:hidden">
                  <button
                      className="flex items-center px-3 py-2 border rounded text-teal-200 border-teal-400 hover:text-white hover:border-white">
                      <svg className="fill-current h-3 w-3" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                          <title>Menu</title>
                          <path d="M0 3h20v2H0V3zm0 6h20v2H0V9zm0 6h20v2H0v-2z"/>
                      </svg>
                  </button>
              </div>
              <div class="w-full block flex-grow lg:flex lg:items-center lg:w-auto">
                  <div class="text-sm lg:flex-grow text-[var(--color-b)]">
                      <Link href="/timeline">
                            <button
                         className="block mt-4 lg:inline-block lg:mt-0 hover:text-white mr-4">
                          Timeline
                            </button>
                      </Link>
                      <Link href="/add">
                          <button
                              className="block mt-4 lg:inline-block lg:mt-0 hover:text-white mr-4">
                          Add Task
                          </button>
                      </Link>
                  </div>
              </div>
          </nav>
      </header>
      <main className={styles.main}>
          <div class="bg-[white] rounded-lg w-1/2 h-[100vh] self-center z-10 grid grid-rows-[min-content_max-content]" >
              <Component/>
          </div>
      </main>

      <footer className={styles.footer}>
      </footer>
    </div>
  )
}

export async function getServerSideProps(context) {
    const {pageName} = context.query
    return {
        props: {
            pageName
        },
    }
}