import nc from "next-connect";
const { Task } = require("../../../models");
const {createResponse,createError} = require("../../../utils");
const handler = nc();

handler.post(async (req, res) => {
    const { title,description, status, priority, storyPoints } = req.body;
    try {
        const task = await Task.create({
            title,
            description,
            status,
            priority,
            storyPoints
        });
        return createResponse(res, task)
    } catch (error) {
        return createError(res, {message: error.message}, 500)
    }
})

export default handler