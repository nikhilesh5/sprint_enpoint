import nc from "next-connect";
const { Task } = require("../../../models");
const {createResponse} = require("../../../utils");
const handler = nc();
handler.get(async (req, res) => {
    const allTasks = await Task.findAll()
    return createResponse(res, allTasks)
})
export default handler